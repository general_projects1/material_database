import setuptools

with open('README.md', "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='simauto_module_material_database',
    version='0.1',
    author="Nathan Gerhardt",
    author_email="nathan.gerhardt@scania.com",
    description=" Package for automated evaluation of engine brackets",
    install_requires=['jinja2',
                      'pyyaml',
                      'numpy',
                      'pandas'
                      ],
    dependency_links=['https://af.scania.com/artifactory/api/pypi/simautomation-pypi-local/simple'],
    license='GPLv3',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.scania.com/simautomation/autoprocess/modules/simauto_module_material_database",
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'material_database=materialdb.material_db:main',
        ],
    },
    include_package_data=True,
    python_requires='>=3.6.8',
)
