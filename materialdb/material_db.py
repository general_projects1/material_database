#!/usr/bin/python3
# Python Packages Import
import os
import pathlib
import sys
import datetime
import json
import sqlite3 as sql

# Material Database Module Extensions
from materialdb.lib.general.yaml_loader import yaml_load
from materialdb.lib.general.app_tools import get_file_list
from materialdb.lib.database_tools.MaterialDatabaseStringModel import MaterialModelTable

# Source File Module Extensions
from materialdb.lib.Material_data_tools.read_material_data_xlsx import *

# Get location of this file relative to custom Library
__this_file__ = pathlib.Path(__file__)
__this_path__ = __this_file__.parent.resolve()

# Default_config_file
default_config_file = __this_path__.joinpath("config.yaml")

# Current Year and Week Number
date = datetime.date.today()
year_week = date.strftime("%Ycw%V")
# Load configurations
config = yaml_load(default_config_file)
master_material_db_dir = pathlib.Path(config["MASTER_DIR"][0])

# Version Release
VERSION = 1.0


def main():
    update_database = False
    material_database = None
    material_database_list = get_file_list(path=master_material_db_dir, search_term="*.db3")

    if not material_database_list:
        material_database = master_material_db_dir.joinpath(f"{config['MASTER_MATERIAL_DB_NAME'][0]}_{year_week}.db3")
    elif len(material_database_list) > 1:
        print("Error - more than one material database exists in the current directory...exiting")
        sys.exit()
    elif len(material_database_list) == 1:
        material_database = master_material_db_dir.joinpath(material_database_list[0])
        update_database = True
    else:
        print(material_database_list)
        print("Error - an unknown exception occurred, check database state or whether it exists...exiting")

    if not material_database:
        print("Database not defined...exiting")
        sys.exit()

    MaterialDatabase(material_database, update_database)


class MaterialDatabase:
    def __init__(self, material_database_path, update_database=True):
        # Set global class literals
        self.update_database = update_database
        self.material_database_path = str(material_database_path)
        self.db_connection = None
        self.database_model = MaterialModelTable()

        # Creates connection to the Database
        self.__get_connection__()

        # Handles whether to construct the database or update records
        if self.update_database:
            print("Updating database...")
            self.__add_update_records__()

        else:
            print("Creating new database and populating with data.")
            self.__create_database__()
            self.__add_update_records__()

    def __get_connection__(self):
        # Checks whether the Material database can either be created or connected to.
        try:
            print(self.material_database_path + "\n\n")
            self.db_connection = sql.connect(self.material_database_path)
        except ConnectionError:
            raise ConnectionError("Could not connect to database, check status of Database")

    def __add_update_records__(self):
        source_files = self.__get_source_material_data__()


    def __create_database__(self):
        self.database_model.__get_database_connection__(self.db_connection)
        self.database_model.__create_tables__()

        return

    def __get_existing_database_records__(self):
        pass

    def __get_source_material_data__(self):
        source_files = config["GROUP_MATERIAL_DEFINITION_LINKS"]
        source_data = {}

        for idx, data_file in enumerate(source_files):
            if pathlib.Path(data_file).exists():
                print(data_file)
                source_data[idx] = read_source_file(data_file, config["SOURCE_FILE_FORMAT"])

            elif not pathlib.Path(data_file).exists():
                del source_files[idx]
            else:
                print("WARNING - an unknown conditional occurred when assessing existence of files")

        return source_files


if __name__ == '__main__':
    main()
