# Import Python relevant packages
from sqlalchemy import Column, Integer, String, PrimaryKeyConstraint, ForeignKey, Table, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship, backref
from sqlalchemy.ext.declarative import declarative_base


Base_class = declarative_base()


material_definition = Table(
    "Material_definition",
    Base_class.metadata,
    Column("Material_ID", Integer, PrimaryKeyConstraint, UniqueConstraint),
    Column("Material_Name", String, UniqueConstraint),
    Column("Material_Type", String),
    Column("")
)


class Base(declarative_base):
    pass


class MaterialDefinition(Base):
    __tablename__ = "MaterialDefinition"

    MaterialID: Mapped[int] = mapped_column(primary_key=True)
    Material_name: Mapped[str] = mapped_column(String(40), nullable=False, unique=True)
    Material_type: Mapped[str] = mapped_column(String(40), nullable=False)
    Elastic_iso_modulus: Mapped[float] = mapped_column(nullable=True)
    Density: Mapped[float] = mapped_column(nullable=False)
    Poisson: Mapped[float] = mapped_column(nullable=False)
    Elastic_ortho_modulus_1: Mapped[float] = mapped_column(nullable=True)
    Elastic_ortho_modulus_2: Mapped[float] = mapped_column(nullable=True)

    def __repr__(self) -> str:
        return f"Material: Material ID={self.MaterialID!r}, Name={self.Material_name!r}"


class GeometricDefinition(Base):
    __tablename__ = "GeometricDefinition"

    GeometricID: Mapped[int] = mapped_column(primary_key=True)
    Material_id: Mapped[int] = mapped_column(ForeignKey("MaterialDefinition.MaterialID"))
    Area: Mapped[str] = mapped_column(String(40), nullable=False)
    Quality: Mapped[str] = mapped_column(String(50), nullable=False)
    FormFactor: Mapped[float] = mapped_column(nullable=False)
    Static_id: Mapped[int] = mapped_column(ForeignKey("StaticDefinition.StaticID"))
    Fatigue_id: Mapped[int] = mapped_column(ForeignKey("FatigueDefinition.FatigueID"))

    def __repr__(self) -> str:
        return f"Geometric Properties: Geo ID={self.GeometricID!r}, Coupled material ID={self.Material_id!r}"


class StaticDefinition(Base):
    __tablename__ = "StaticDefinition"

    StaticID: Mapped[int] = mapped_column(primary_key=True)
    


