

class MaterialModelTable:
    def __init__(self):
        self.connection = None

    @staticmethod
    def __material_definition__():

        return """CREATE TABLE MaterialDefinition(
                MaterialID INTEGER UNIQUE NOT NULL PRIMARY KEY,
                MaterialName TEXT UNIQUE NOT NULL,
                MaterialType TEXT NOT NULL,
                RecordRevision FLOAT NOT NULL,
                CONSTRAINT MaterialUnicity
                UNIQUE(MaterialID, MaterialName) ON CONFLICT FAIL)"""

    @staticmethod
    def __elastic_definition__():

        return """CREATE TABLE ElasticDefinition(
                ElasticID INTEGER UNIQUE NOT NULL PRIMARY KEY,
                materialID INTEGER UNIQUE NOT NULL,
                ElasticModulus_ISO REAL,
                ShearModulus_ISO REAL,
                Density REAL NOT NULL,
                Poisson_ISO REAL NOT NULL,
                ElasticModulus_1_ORTHO REAL,
                ElasticModulus_2_ORTHO REAL,
                ShearModulus_12_ORTHO REAL,
                ShearModulus_21_ORTHO REAL,
                Poisson_ORTHO_12 REAL,
                Poisson_ORTHO_21 REAL,
                FOREIGN KEY(materialID) REFERENCES MaterialDefinition(MaterialID),
                CONSTRAINT ElasticUnicity
                UNIQUE(ElasticID, materialID) ON CONFLICT FAIL)"""

    @staticmethod
    def __plastic_definition__():

        return """CREATE TABLE PlasticDefinition(
                PlasticID INTEGER UNIQUE NOT NULL PRIMARY KEY,
                materialID INTEGER NOT NULL,
                PlasticModelType TEXT NOT NULL,
                PlasticOrderID INTEGER NOT NULL,
                Stress REAL NOT NULL,
                Strain REAL NOT NULL,
                PlastificationTemperature REAL NOT NULL,
                FOREIGN KEY(materialID) REFERENCES MaterialDefinition(MaterialID),
                CONSTRAINT PlasticUnicity
                UNIQUE(PlasticID, materialID, PlasticModelType,PlasticOrderID, PlastificationTemperature) ON CONFLICT FAIL)"""

    @staticmethod
    def __geometric_definition__():

        return """CREATE TABLE GeometricDefinition(
                GeometricID INTEGER UNIQUE NOT NULL PRIMARY KEY,
                materialID INTEGER NOT NULL,
                AreaType TEXT NOT NULL,
                Quality TEXT NOT NULL,
                FormFactor REAL NOT NULL,
                staticID INTEGER NOT NULL,
                fatigueID INTEGER NOT NULL,
                FOREIGN KEY(materialID) REFERENCES MaterialDefinition(MaterialID),
                FOREIGN KEY(staticID) REFERENCES StaticDefinition(StaticID),
                FOREIGN KEY(fatigueID) REFERENCES FatigueDefinition(FatigueID),
                CONSTRAINT GeometricUnicity
                UNIQUE(GeometricID, materialID, staticID, fatigueID) ON CONFLICT FAIL)"""

    @staticmethod
    def __static_definition__():

        return """CREATE TABLE StaticDefinition(
                StaticID INTEGER UNIQUE NOT NULL PRIMARY KEY,
                materialID INTEGER NOT NULL,
                UltimateTensileStrength REAL NOT NULL,
                YieldStrength REAL NOT NULL,
                Strain_at_Yield REAL NOT NULL,
                Strain_at_Break REAL NOT NULL,
                FOREIGN KEY(materialID) REFERENCES MaterialDefinition(MaterialID),
                CONSTRAINT StaticUnicity
                UNIQUE(StaticID, materialID) ON CONFLICT FAIL)"""

    @staticmethod
    def __fatigue_definition__():

        return """CREATE TABLE FatigueDefinition(
                FatigueID INTEGER UNIQUE NOT NULL PRIMARY KEY,
                materialID INTEGER NOT NULL,
                NoCycles REAL NOT NULL,
                StressR_ReversedLoad REAL NOT NULL,
                StressR_Pulsating REAL NOT NULL,
                FOREIGN KEY(materialID) REFERENCES MaterialDefinition(MaterialID),
                CONSTRAINT FatigueUnicity
                UNIQUE(FatigueID, materialID, NoCycles) ON CONFLICT FAIL)
                """

    def __get_database_connection__(self, database_connection):
        self.connection = database_connection

        return

    def __create_tables__(self):
        self.connection.execute(self.__material_definition__())
        self.connection.execute(self.__elastic_definition__())
        self.connection.execute(self.__plastic_definition__())
        self.connection.execute(self.__static_definition__())
        self.connection.execute(self.__fatigue_definition__())
        self.connection.execute(self.__geometric_definition__())
