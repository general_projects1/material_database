import pandas as pd
import numpy as np


def read_source_file(source_file, source_format=None):
    data_frame = pd.read_excel(source_file, engine="openpyxl", sheet_name=source_format, skiprows=1, index_col=None, header=None)
    source_dictionary = {}
    for sheet in source_format:
        shape = data_frame[sheet].shape
        numpy_array = data_frame[sheet].dropna().to_numpy()

        material_combination_count = 1
        for idx, row in enumerate(numpy_array):
            if idx == 0:
                print("Headings")
                print(row)
                print("\n")
                continue

            if row[0] != numpy_array[idx - 1][0]:
                material_combination_count = 1

            if not row[0] in source_dictionary.keys():
                source_dictionary[row[0]] = {}

            source_dictionary[row[0]][material_combination_count] = {
                "MaterialType": row[1],
                "MaterialUniformity": row[2],
                "MaterialClass": row[3],
                "Area": row[4],
                "FormFactor": row[5],
                "Cycles": row[6],
                "Quality": row[7]
            }

            material_combination_count += 1
        print(source_dictionary)





