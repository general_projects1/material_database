import unittest

class TestStringMethods(unittest.TestCase):

    def test_truth(self):
        """
            Dummy test to verity True
        """
        self.assertTrue(True)

    def test_logic(self):
        """
            Making sure basic logic works.
        """
        self.assertFalse(1==0)

if __name__ == '__main__':
    unittest.main()
